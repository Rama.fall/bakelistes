const cartedisplay = document.getElementById("placeajout");

function afficher(something){
    cartedisplay.innerHTML ="";
    something.forEach(({img, completedName, domaine, duree, coach, mail}) => {

        cartedisplay.innerHTML += `


        <div class="col">
        <div class="card cards py-3 ">
          <img
            src="${img}"
            class="card-img-top w-25 mx-auto rounded-circle "
            alt="..."
          />
          <div class="card-body">
            <div class="text-center text-white mb-3">
              <h5 class="text-primary">${completedName}</h5>
            </div>

            <div
              class="text-white d-flex justify-content-between align-items-center"
            >
              <p class="fw-bold text-uppercase text-decoration-underline">
                Domaine:
              </p>
              <p>${domaine}</p>
            </div>

            <div
              class="text-white d-flex justify-content-between align-items-center"
            >
              <p class="fw-bold text-uppercase text-decoration-underline">
                Duree de formation:
              </p>
              <p>${duree}</p>
            </div>
            <div
              class="text-white d-flex justify-content-between align-items-center"
            >
              <p class="fw-bold text-uppercase text-decoration-underline">
                Coach:
              </p>
              <p>${coach}</p>
            </div>
            <div
              class="text-white d-flex justify-content-between align-items-center"
            >
              <p class="fw-bold text-uppercase text-decoration-underline">
                mail:
              </p>
              <p>${mail}</p>
            </div>
          </div>
        </div>
      </div>

        `



    })
}



async function recupererJson(){
    const response = await fetch('data.json');
    const data = await response.json();
    return data;
  }
  
  async function displayCards() {
    const res = await recupererJson();
    console.log({ res });
    afficher(res);
  }
  
  displayCards();